<?php
	header('Content-type: application/json');
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    $request = json_decode(file_get_contents("php://input"));

	$recaptcha = $request->recaptcha;
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$data = array(
		'secret' => '',
		'response' => $recaptcha
	);
	$options = array(
		'http' => array (
			'method' => 'POST',
            'Content-Type: application/x-www-form-urlencoded',
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$verify = file_get_contents($url, false, $context);
	$captcha_success = json_decode($verify);

	if ($captcha_success->success) {
		$nombre = $request->name;
		$mail = $request->email;
		$telefono = $request->telephone;
		$message = $request->message;
		$job = $request->job;
		$company = $request->company;

		$header = 'From: ' . $mail . " \r\n";
		$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
		$header .= "Mime-Version: 1.0 \r\n";
		$header .= "Content-Type: text/plain";

		$mensaje = "\r\nEste mensaje fue enviado por " . $nombre . "\r\n";
		$mensaje .= "Asunto: Contacto por web\r\n";
		$mensaje .= "Telefono: " . $telefono . " \r\n";
		if (!empty($job)) {
			$mensaje .= "Cargo: " . $job . " \r\n";
		}
		if (!empty($company)) {
			$mensaje .= "Compañia: " . $company . " \r\n";
		}
		$mensaje .= "Su e-mail es: " . $mail . " \r\n";
		$mensaje .= "Mensaje: " . $message . " \r\n";
		$mensaje .= "Enviado el " . date('d/m/Y', time());

		$para = 'blackblex@gmail.com';
		$asunto = 'Contacto desde la Pagina Web';

        $response_array['status'] = 'success';
        $response_array['from'] = $mail;

        echo json_encode($response_array);
        header("status: " . $response_array['status']);
        header("from: " . $response_array['from']);
	} else {
	 	echo json_encode(['status', 'failed']);
        header("status: failed");
	}
?>
