import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { HomeComponent } from './home/home.component';
import { ServicesRoutingModule } from './services-routing.module';


@NgModule({
	declarations: [
		HomeComponent
	],
	imports: [
		CommonModule,
		ServicesRoutingModule
	]
})
export class ServicesModule { }
