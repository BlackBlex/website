import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
	selector: 'app-contact-form',
	templateUrl: './form.component.html',
	styleUrls: ['./form.component.css']
})
export class FormComponent {
	form: FormGroup;

	status: string = '';

	constructor(private formBuilder: FormBuilder, private http: HttpClient) {
		this.form = this.formBuilder.group({
			name: ['', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-Z ]*')]],
			email: ['', [Validators.required, Validators.email]],
			telephone: [
				'',
				[Validators.required, Validators.pattern('[0-9]{10}')],
			],
			message: ['', [Validators.required, Validators.maxLength(500)]],
			recaptcha: [null, [Validators.required]],
			job: ['', []],
			company: ['', []]
		});
	}

	public save(event: Event): void {
		event.preventDefault();

		if (this.form.valid) {
			const value = this.form.value;
			console.log(JSON.stringify(value));
			this.http
				.post('http://localhost/send_mail.php', JSON.stringify(value))
				.subscribe({
					next: (v) => {
						let jsonObj: any = JSON.parse(JSON.stringify(v));

						if (jsonObj.status == 'success') {
							this.status =
								'Mensaje enviado, en breve nos comunicamos con usted';
						} else {
							this.status =
								'Ha ocurrido un error al enviar el mensaje';
						}
					},
				});
		} else {
			this.form.markAllAsTouched();
		}
	}

	get formControls() {
		return this.form.controls;
	}
}
