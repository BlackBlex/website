import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
	RecaptchaFormsModule,
	RecaptchaModule,
	RecaptchaSettings,
	RECAPTCHA_SETTINGS
} from 'ng-recaptcha';
import { environment } from 'src/environments/environment';
import { ContactRoutingModule } from './contact-routing.module';
import { FormComponent } from './form/form.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
	declarations: [
		HeaderComponent,
		FormComponent
	],
	imports: [
		CommonModule,
		ContactRoutingModule,
		ReactiveFormsModule,
		RecaptchaModule,
		RecaptchaFormsModule,
		HttpClientModule,
	],

	providers: [
		{
			provide: RECAPTCHA_SETTINGS,
			useValue: {
				siteKey: environment.recaptcha.siteKey,
			} as RecaptchaSettings,
		},
	],
})
export class ContactModule { }
