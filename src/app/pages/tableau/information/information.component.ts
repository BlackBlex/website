import { Component, ElementRef, ViewChild } from '@angular/core';

@Component({
	selector: 'app-tableau-information',
	templateUrl: './information.component.html',
	styleUrls: ['./information.component.css']
})
export class InformationComponent {
	@ViewChild('openModalButton')
	openModal!: ElementRef<HTMLButtonElement>;
	@ViewChild('imageToPreview')
	imageToPreview!: ElementRef<HTMLImageElement>;

	public previewImage(event: Event): void {
		if (event) {
			let imageTag = (event.target as HTMLImageElement);
			this.imageToPreview.nativeElement.src = imageTag.src;
			this.openModal.nativeElement.click();
		}
	}

	public scrollPoint(element: HTMLElement): void {
		element.scrollIntoView({ behavior: "smooth" });
	}
}
