import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { HeaderComponent } from './header/header.component';
import { InformationComponent } from './information/information.component';
import { TableauRoutingModule } from './tableau-routing.module';


@NgModule({
	declarations: [
		HeaderComponent,
		InformationComponent
	],
	imports: [
		CommonModule,
		TableauRoutingModule
	]
})
export class TableauModule { }
