import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from './post.model';

@Injectable({
	providedIn: 'root'
})
export class PostService {
	posts: Post[] = [{
		id: 1,
		image: "assets/images/blog/1.jpg",
		title: "Cómo hacer la interpretación de datos",
		subtitle: "Los datos en bruto son inútiles si no se interpretan. Aprende su importancia y como hacerlo",
		description: "La interpretación de datos te ayudarán, como gestor, a tomar una decisión informada basada en los números y con todos los datos a tu disposición",
		text: `<h2><b>¿Qué es la interpretación de datos?</b></h2>
		<p>La interpretación de datos es el proceso de revisar los datos y llegar a conclusiones relevantes utilizando varios métodos analíticos.El <a href="./blog/5">análisis de datos</a> ayuda a los investigadores a categorizar, manipular y resumir los datos para responder a preguntas críticas.</p>
		<p>En términos empresariales, la interpretación de datos es la ejecución de varios procesos.Este proceso analiza y revisa los datos para obtener conclusiones y reconocer patrones y comportamientos emergentes.Estas conclusiones te ayudarán, como gestor, a tomar una decisión informada basada en los números y con todos los datos a tu disposición.</p>
		<h3><b>Importancia de la interpretación de los datos</h3></b>
		<p>Los datos en bruto son inútiles si no se interpretan.La interpretación de datos es importante para las empresas y las personas.Los datos recogidos ayudan a:</p>
		<h4><b>Tomar mejores decisiones</b></h4>
		<p class="ps-5">Cualquier decisión se basa en la información disponible en ese momento.Cuando se sabe recoger y comprender bien los datos, se pueden tomar mejores decisiones.Puedes elegir con confianza un camino para tu organización o incluso para tu vida en lugar de trabajar con suposiciones.</p>
		<p>Lo más importante es seguir un proceso transparente para reducir los errores y el cansancio al tomar decisiones.</p>
		<h4><b>Encontrar tendencias y tomar medidas</b></h4>
		<p class="ps-5">Otro uso práctico de la interpretación de datos es adelantarse a las tendencias antes de que alcancen su punto álgido.Algunas personas se han ganado la vida investigando sectores, detectando tendencias y haciendo luego grandes apuestas sobre ellas.Con las interpretaciones de datos adecuadas y un poco de trabajo, puedes captar el inicio de las tendencias y utilizarlas para ayudar a tu negocio o a ti mismo a crecer.</p>
		<h4><b>Mejor asignación de recursos</b></h4>
		<p class="ps-5">La última importancia de la interpretación de datos de la que hablaremos es la capacidad de utilizar personas, herramientas, dinero, etc., de forma más eficiente.Gracias a una correcta interpretación de datos puedes descubrir que un mercado que creía que encajaba bien es en realidad malo.Esto puede deberse a que el mercado es demasiado grande para tus productos, a que hay demasiada competencia o a cualquier otra cosa.</p>
		<p>Sea como fuere, puedes mover los recursos que necesites más rápido y mejor para obtener mejores resultados.</p>
		<h3><b>Informes de resultados ¿Cómo hacerlo?</b></h3>
		<p>El nivel y el alcance del contenido depende de a quién se destina el informe, por ejemplo, a los financiadores / banqueros, empleados, clientes, el público, etc.</p>
		<p>Asegúrese de que los empleados tengan la oportunidad de revisar y analizar cuidadosamente el informe.Traduzca las recomendaciones a los planes de acción, incluido quién hará qué con los resultados de la investigación y cuándo.</p>
		<p>Los financiadores / banqueros probablemente requerirán un informe que incluya un resumen ejecutivo(este es un resumen de las conclusiones y recomendaciones, no una lista de qué secciones de información están en el informe, eso es una tabla de contenido)</p>
		<p>También querrán:</p>
		<p class="ps-5 pb-0 mb-0">Una descripción de la organización y el programa, producto, servicio, etc., en evaluación.
		</p>
		<p class="ps-5 pb-0 mb-0">Explicación de los objetivos, métodos y procedimientos de análisis de la investigación</p>
		<p class="ps-5 pb-0 mb-0">Listado de conclusiones y recomendaciones</p>
		<p>Y cualquier archivo adjunto relevante, por ejemplo, la inclusión de cuestionarios de investigación, guías de entrevistas, etc.</p>
		<p>El financiador puede desear que el informe se entregue como una presentación, acompañado de una descripción general del informe.O, el financiador puede querer revisar el informe solo</p>`,
		references: [],
		author: "Website user",
		date: "29/11/2022"
	}, {
		id: 2,
		image: "assets/images/blog/2.jpg",
		title: "¿Qué es un CRM y para que sirve?",
		subtitle: "Conoce que es un CRM y como puede ayudar al crecimiento de tu empresa",
		description: "Contando con un sistema de CRM, las empresas de todos los tamaños pueden mantenerse conectadas con los clientes, optimizar los procesos, mejorar la rentabilidad e impulsar el crecimiento del negocio",
		text: "",
		references: [],
		author: "Website user",
		date: "28/11/2022"
	}, {
		id: 3,
		image: "assets/images/blog/3.jpg",
		title: "Business Intelligence en el sector salud",
		subtitle: "Descubre como es que el sector Salud se beneficia de herramientas como el Business Intellgience",
		description: "Tableau es una herramienta intuitiva que te permite visualizar datos y atacar los retos que presenta el sector salud mejorando la eficiencia y eficacia de la organización",
		text: "",
		references: [],
		author: "Website user",
		date: "27/11/2022"
	}, {
		id: 4,
		image: "assets/images/blog/4.jpg",
		title: "Transformación digital con Análisis de Datos",
		subtitle: "Comienza la transformación digital de tu empresa con análisis de datos",
		description: "Aprovecha los datos de tu empresa y conviértelos en información que te permita tomar decisiones basadas en hechos reales",
		text: "",
		references: [],
		author: "Website user",
		date: "26/11/2022"
	}, {
		id: 5,
		image: "assets/images/blog/5.jpg",
		title: "Business intelligence-5 Tips para el Analisis de datos",
		subtitle: "Comprenda sus datos sin importar la cantidad ni el lugar en el que estén almacenados",
		description: "El mundo de los datos esta en incremento, utiliza los siguientes Tips para el análisis de datos",
		text: "",
		references: ["https://grupokorporate.com/como-convertirse-en-una-empresa-data-driven-5-tips-imprescindibles-que-te-llevaran-al-exito-seguro/"],
		author: "Website user",
		date: "25/11/2022"
	}];

	constructor() { }

	public getPosts(): any {
		const postsObservable = new Observable(observer => {
			setTimeout(() => {
				observer.next(this.posts);
			}, 500);
		});

		return postsObservable;
	}
}
