export class Post {
	id: Number = -1;
	image: string = "";
	title: string = "";
	subtitle: string = "";
	description: string = "";
	text: string = "";
	references: Array<string> = [""];
	author: string = "";
	date: string = "";
}
