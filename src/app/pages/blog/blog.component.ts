import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Post } from './post.model';
import { PostService } from './post.service';

@Component({
	selector: 'app-blog',
	templateUrl: './blog.component.html',
	styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
	@ViewChild('openModalButton')
	openModal!: ElementRef<HTMLButtonElement>;
	@ViewChild('postTitle')
	postTitle!: ElementRef<HTMLParagraphElement>;
	@ViewChild('postDescription')
	postDescription!: ElementRef<HTMLLabelElement>;
	@ViewChild('postImage')
	postImage!: ElementRef<HTMLImageElement>;
	@ViewChild('postAuthor')
	postAuthor!: ElementRef<HTMLLabelElement>;
	@ViewChild('postDate')
	postDate!: ElementRef<HTMLLabelElement>;
	@ViewChild('postText')
	postText!: ElementRef<HTMLDivElement>;
	@ViewChild('postReferences')
	postReferences!: ElementRef<HTMLDivElement>;
	@ViewChild('postShareFacebook')
	postShareFacebook!: ElementRef<HTMLAnchorElement>;
	@ViewChild('postShareMail')
	postShareMail!: ElementRef<HTMLAnchorElement>;
	@ViewChild('postShareTwitter')
	postShareTwitter!: ElementRef<HTMLAnchorElement>;
	@ViewChild('postShareLinkedin')
	postShareLinkedin!: ElementRef<HTMLAnchorElement>;

	private sub: any;

	selectedPost: Post | undefined;
	posts: Post[] = [];

	urlTwitter: string = "https://twitter.com/intent/tweet?text=";
	urlFacebook: string = "https://www.facebook.com/sharer/sharer.php?u=";
	urlMail: string = "mailto:?subject={title}&body={subtitle}<br>Leer Más en {url}";
	urlLinkedin: string = "https://www.linkedin.com/sharing/share-offsite/?url=";

	constructor(private Postservice: PostService, @Inject(DOCUMENT) private document: Document, private route: ActivatedRoute) { }

	ngOnInit(): void {
		const PostObservable = this.Postservice.getPosts();
		PostObservable.subscribe((PostsData: Post[]) => {
			this.posts = PostsData;
		});

		setTimeout(() => {
			this.sub = this.route.params.subscribe(params => {
				this.previewPost(+params['id']);
			});
		}, 600);

	}

	public previewPost(id: Number): void {
		this.selectedPost = this.posts
			.find((p: Post) => p.id === id);

		console.log(this.document.location.origin);
		console.log(this.document.location.href);
		this.postReferences.nativeElement.textContent = "";
		if (this.selectedPost) {
			this.postTitle.nativeElement.textContent = this.selectedPost.title;
			this.postImage.nativeElement.src = this.selectedPost.image;
			this.postImage.nativeElement.alt = this.selectedPost.title;
			this.postDescription.nativeElement.textContent = this.selectedPost.description;
			this.postAuthor.nativeElement.textContent = this.selectedPost.author;
			this.postDate.nativeElement.textContent = this.selectedPost.date;
			this.postText.nativeElement.innerHTML = this.selectedPost.text;

			if (this.selectedPost.references.length !== 0) {
				this.postReferences.nativeElement.innerHTML = "Referencias: <br>";
				this.selectedPost.references.forEach(reference => {
					this.postReferences.nativeElement.innerHTML += reference;
				})

			}

			this.postShareFacebook.nativeElement.href = this.urlFacebook + this.document.location.origin + "/blog/" + this.selectedPost.id;

			this.postShareTwitter.nativeElement.href = this.urlTwitter + this.document.location.origin + "/blog/" + this.selectedPost.id;

			this.postShareLinkedin.nativeElement.href = this.urlLinkedin + this.document.location.origin + "/blog/" + this.selectedPost.id;

			this.postShareMail.nativeElement.href = this.urlMail.replace("{title}", this.selectedPost.title).replace("{subtitle}", this.selectedPost.subtitle).replace("{url}", this.document.location.origin + "/blog/" + this.selectedPost.id);

			this.openModal.nativeElement.click();
		}
	}

}
