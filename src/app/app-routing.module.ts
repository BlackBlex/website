import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { BlogComponent } from './pages/blog/blog.component';
import { HomeComponent } from './pages/home/home.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

const routes: Routes = [
	{ path: '', redirectTo: '/home', pathMatch: 'full' }, { path: 'home', component: HomeComponent },
	{ path: 'aboutus', component: AboutUsComponent },
	{
		path: 'contact',
		loadChildren: () =>
			import('./pages/contact/contact.module').then((m) => m.ContactModule),
	},
	{ path: 'blog', component: BlogComponent },
	{ path: 'blog/:id', component: BlogComponent },
	{
		path: 'services',
		loadChildren: () =>
			import('./pages/services/services.module').then((m) => m.ServicesModule),
	},
	{
		path: 'tableau',
		loadChildren: () =>
			import('./pages/tableau/tableau.module').then((m) => m.TableauModule),
	},
	{
		path: '**', pathMatch: 'full',
		component: NotFoundComponent
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
