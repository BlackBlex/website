import { Component } from '@angular/core';

@Component({
	selector: 'app-navigation',
	templateUrl: './navigation.component.html',
	styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {
	isMovil: string = 'undefined';

	navItems: Array<Array<String>> = [
		['Inicio', '/home'],
		['Servicios', '/services'],
		['Tableau', '/tableau'],
		['Blog', '/blog'],
		['Contacto', '/contact'],
	];

	constructor() { }

	ngOnInit(): void { }

	setMovil(): void {
		this.isMovil = 'collapse';
	}

	removeMovil(): void {
		this.isMovil = 'undefined';
	}

}
